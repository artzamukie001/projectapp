import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, Alert, TextInput, Button, ImageBackground } from 'react-native';


class Login extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <View style={styles.row}>
                        <View style={styles.column}>
                            <Text style={styles.text}>MoneyGo</Text>
                            <View style={styles.image}>
                                <Image
                                    style={styles.image}
                                    source={{ uri: 'https://www.bpicc.com/images/2019/02/19/pug.jpg' }}
                                />

                            </View>
                        </View>

                    </View>
                </View>

                <View style={styles.content}>
                    <View style={styles.column}>
                        <View style={styles.inputtex}>
                            <TextInput style={styles.text1}

                                placeholder='username'
                            />
                        </View>
                        <View style={styles.inputtex}>
                            <TextInput style={styles.text1}

                                placeholder='password'
                            />
                        </View>

                        <View style={styles.column}>
                            <TouchableOpacity>
                                <Button title="Login" onPress={() => { this.onShowAlert() }} />
                            </TouchableOpacity>
                        </View>

                        <View style={styles.column}>
                            <TouchableOpacity>
                                <Button title="Login with Facebook" onPress={() => { this.onShowAlert() }} />
                            </TouchableOpacity>
                        </View>


                    </View>
                </View>

            </View>

        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'red',
        flex: 1
    },
    header: {
        backgroundColor: '#99FFCC',
        flex: 1
    },
    content: {
        backgroundColor: '#99FFCC',
        flex: 1
    },
    row: {
        backgroundColor: '#99FFCC',
        flexDirection: 'row',
        alignItems: 'center',
        flex: 1,
    },
    column: {
        backgroundColor: '#99FFCC',
        flexDirection: 'column',
        alignItems: 'center',
        flex: 1,
    },
    text: {
        fontSize: 50,
        fontWeight: 'bold',
        padding: 20
    },
    text1: {
        fontSize: 20,
        fontWeight: 'bold',
        padding: 20
    },
    image: {
        backgroundColor: '#99FFCC',
        width: 150,
        height: 150,
        borderRadius: 100,
    },
    inputtex: {
        backgroundColor: 'white',
        flexDirection: 'column',
        alignItems: 'center',
        width: 300,
        height: 60,
        margin: 14,
    },
})

export default Login
